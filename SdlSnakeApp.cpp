#include <SDL2/SDL.h>
#include "SdlSnakeApp.h"
#include "Configuration.h"
#include "Snake.h"

SdlSnakeApp::SdlSnakeApp(Configuration& refConfiguration, const char* szTitle)
: myConfiguration(refConfiguration), mySnake(refConfiguration), szAppTitle(szTitle)
{
}

bool SdlSnakeApp::init()
{
    // Initialization return value
	bool success = true;

     // make sure SDL cleans up before exit
    atexit(SDL_Quit);

	// Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL_Init() failed. SdlSnakeApp could not be initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		//Create window
		myWindow = SDL_CreateWindow( "SdlSnake", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, myConfiguration.getWinWidth(), myConfiguration.getWinHeight(), SDL_WINDOW_SHOWN );
		if( myWindow == NULL )
		{
			printf( "SDL_CreateWindow() failed. Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
            SDL_SetWindowTitle(myWindow, szAppTitle);
			mySurfaceWinodow = SDL_GetWindowSurface(myWindow);
		}
	}

	return success;
}

bool SdlSnakeApp::loadMedia()
{
    bool areAllFilesLoaded = true;
    // load an image
    mySurfaceSnake = SDL_LoadBMP("media/TruckFront_78x57.bmp");
    if (!mySurfaceSnake)
    {
        printf("Unable to load bitmap: %s\n", SDL_GetError());
        areAllFilesLoaded = false;
    }
    else
    {
        mySnake.setSnakeHeadSize(mySurfaceSnake->w, mySurfaceSnake->h);
    }
    
    return areAllFilesLoaded;
}

bool SdlSnakeApp::processEvents()
{
    bool isProcessEventsOk = true;

    // message processing loop
    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
        // Handle events for the main window.
        switch (event.type)
        {
            // Exit if the window is closed.
        case SDL_QUIT:
            bQuitGame = true;
            break;

            // Check for keypresses
        case SDL_KEYDOWN:
            printf("Key d%02d\n", event.key.keysym.sym);
            fflush(stdout);

            // exit if ESCAPE is pressed
            if (event.key.keysym.sym == SDLK_ESCAPE)
            {
                bQuitGame = true;
            }
            break;

        default:
            /* Notthing to do.*/
            break;

        } // End switch

        // Handle Events at all other objects.
        mySnake.processEvents(event);

    } // end of message processing

    return isProcessEventsOk;
}

bool SdlSnakeApp::updateGame()
{
    bool bUpdateGameOk = true;

    if (isNewGame())
    {
        mySnake.restartSnake();

        bNewGame = false;
    }

    // move the snake after timer has expired
    static int previousTimerTicks = 0; 
    int currentTimerTicks = 0;

    currentTimerTicks = SDL_GetTicks();
    if (currentTimerTicks - previousTimerTicks > 20)
    {
        // Move all movable objects;
        mySnake.move(myFood);

        previousTimerTicks = currentTimerTicks;
    }
    else
    {
        SDL_Delay(5);
    }

    return bUpdateGameOk;    
}

bool SdlSnakeApp::renderScreen()
{
    bool bRenderScreenOk = true;

    // clear window
    SDL_FillRect(mySurfaceWinodow, 0, SDL_MapRGB(mySurfaceWinodow->format, 0, 0, 0));

    // Draw all objects
    SDL_BlitSurface(mySurfaceSnake, 0, mySurfaceWinodow, &(const_cast<SDL_Rect&>(mySnake.getCollider())));
 
    // Finally, update the window.
    SDL_UpdateWindowSurface(myWindow);

    return bRenderScreenOk;
}

void SdlSnakeApp::close()
{
    // free loaded bitmap
    SDL_FreeSurface(mySurfaceSnake);

    // free surface of windowsloaded bitmap
    SDL_FreeSurface(mySurfaceWinodow);

    // Destroy window
    SDL_DestroyWindow(myWindow);

    // Quit SDL subsystem
    SDL_Quit();
}

