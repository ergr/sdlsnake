# SdlSnake

A snake clone. Written in C++.

## Application

    SdlSnake_x86_64.exe

### Usage

Run the executable. Move the "snake" with the keys `UP`, `DOWN`, `LEFT`, `RIGHT`.

## Target systems

1. MS Windows 10
   1. native (build with MSYS2)
   2. WSL
2. Linux
   1. Debian

Further information about Windows and MSYS2 can be found at [msys2.md](doc/msys2.md).

## Build

### Executable

    make

After the build command the executable can be found under `./build`

### Documentation

    make doc

The documentation is formatted in HTML. Open with a browser [./build/doc/html/index.html](../../../build/doc/html/index.html)

### Unittests with Catch2

    make check

## Prerequisite

### Depending libraries

1. SDL2

Visit the makefile. Check the LDLIBS for the complete list of used libraries.

#### To install the libraries on MSYS2

    pacman -S mingw-w64-x86_64-SDL2 mingw-w64-x86_64-SDL2_image

### Toolchains

### To build and debug the executable

1. clang++
2. g++
3. make
4. gdb

#### To install the build toolchain on MSYS2

    pacman -S mingw-w64-x86_64-clang mingw-w64-x86_64-gcc make mingw-w64-x86_64-gdb

### To generate the documentation

1. doxygen
2. graphviz
3. plantuml

#### To install the documentation toolchain on MSYS2

    pacman -S mingw-w64-x86_64-doxygen mingw-w64-x86_64-graphviz

PlantUML uses java
Prepare your `.bashrc` to acces your java installation. Here is an example:

```bash
export JAVA_HOME=/C/Program\ Files/Java/jdk-13.0.2
export PATH=/C/Program\ Files/Java/jdk-13.0.2/bin/:$PATH
```

Download the jar file and put it to folder as configured in the doxygen.conf:

    /usr/local/share/plantuml/plantuml.jar

### To run the unit-tests

The header file `catch.hpp` implements the unit-test framework. Provided by the Catch2 project `https://github.com/catchorg/Catch2`.
The `testmain.cpp` keeps the main of the catch2 unittest framework.
The `SdlSnakeMain.cpp` that implements the `main()` function of the application, hast to be omitted in the makefile when building the unit-tests.
The dedicated unit-tests are written inside the source files with use of the pre-compiler switch `UNIT_TEST`.

### Extras on MS-Windows

1. msys2 with mingw64

## Media files

The media files are created with GIMP [gimp.org](www.gimp.org).

## IDE's and other build options

During development several approaches were evaluated. Maybe not all run anymore and probably need some maintenance to get them up running again.
The latest evaluation was performed with use of VSCode.

1. VSCode     - on Windows with the MSYS2 mingw64
2. CodeBlocks - on Linux i686_32bit (Debian)
    * There is a _SdlSnake.cbp_ project file.
3. VSCode     - on Linux x86_64 (Debian)
4. VSCode     - on Windows with the WSL

## Architecture & Design

An architectural diagram will be provided some time in `zEvalDiagramm.dia`

```dia
\diafile zEvalDiagram.dia
```

\diafile zEvalDiagram.dia

Or a graphviz dot drawing:

```graphviz
digraph G {
    subgraph cluster0 { node [style=filled];
    color=grey;
    state_init [style=bold,shape=box,peripheries=2,color=black,label="init()"];
    state_init -> "loadMedia()" [style=bold];
    "loadMedia()" -> "processEvents()\nupdateGame()\nrenderScreen()"
    "processEvents()\nupdateGame()\nrenderScreen()" -> "processEvents()\nupdateGame()\nrenderScreen()"
    "processEvents()\nupdateGame()\nrenderScreen()" -> "close()"
    fontsize=18;label="State Machine"; }
}
```

\dot
digraph G {
    subgraph cluster0 { node [style=filled];
    color=grey;
    state_init [style=bold,shape=box,peripheries=2,color=black,label="init()"];
    state_init -> "loadMedia()" [style=bold];
    "loadMedia()" -> "processEvents()\nupdateGame()\nrenderScreen()"
    "processEvents()\nupdateGame()\nrenderScreen()" -> "processEvents()\nupdateGame()\nrenderScreen()"
    "processEvents()\nupdateGame()\nrenderScreen()" -> "close()"
    fontsize=18;label="State Machine"; }
}
\enddot

The next lines will generate a preview in VSCode with the `plantuml` extension.
When generating the doxygen output, the next lines will be a code block. This is du to incompatibility of doxygen and vscode markdown extensions. While this is acceptable for now, it is expected that doxygen will adopt in the near future and this text and the duplication of plantuml code can be replaced. (ernst.gross_2020-04-11)

Further information about PlantUML can be found at [plantuml.md](doc/plantuml.md).

```plantuml
@startuml
    skinparam backgroundColor #EEEBDC
    skinparam handwritten false
    SdlSnakeMain "1" *- "1" SdlSnakeApp    : contains >
    SdlSnakeMain *-- Configuration  : contains >
    SdlSnakeApp  *-- Snake          : contains >
    SdlSnakeApp  *-- Configuration  : contains >
    Snake        *-- Configuration  : contains >
    Snake        ..  Utils          : uses >

    Snake        *-- SDL_Rect       : contains >
    SdlSnakeApp  ..  SDL_Rect       : uses >
    Utils        ..  SDL_Rect       : uses >

    SdlSnakeApp  o-- SDL_Window     : aggregates >
    SdlSnakeApp  o-- SDL_Surface    : aggregates >
    SdlSnakeApp  ..  SDL_Event      : uses >

    SdlSnakeApp :  init()
    SdlSnakeApp :  loadMedia()
    SdlSnakeApp :  processEvents()
    SdlSnakeApp :  updateGame()
    SdlSnakeApp :  renderScreen()
    SdlSnakeApp :  close()

    Snake : processEvents()
    Snake : move()

    Utils : checkCollision()
@enduml
```

Keep the next lines for `doxygen` that generates the PlantUML diagram for HTML documentation.

@startuml

    skinparam backgroundColor #EEEBDC
    skinparam handwritten false
    SdlSnakeMain "1" *- "1" SdlSnakeApp    : contains >
    SdlSnakeMain *-- Configuration  : contains >
    SdlSnakeApp  *-- Snake          : contains >
    SdlSnakeApp  *-- Configuration  : contains >
    Snake        *-- Configuration  : contains >
    Snake        ..  Utils          : uses >
    Snake        *-- SDL_Rect       : contains >
    SdlSnakeApp  ..  SDL_Rect       : uses >
    Utils        ..  SDL_Rect       : uses >
    
    SdlSnakeApp  o-- SDL_Window     : aggregates >
    SdlSnakeApp  o-- SDL_Surface    : aggregates >
    SdlSnakeApp  ..  SDL_Event      : uses >
    SdlSnakeApp :  init()
    SdlSnakeApp :  loadMedia()
    SdlSnakeApp :  processEvents()
    SdlSnakeApp :  updateGame()
    SdlSnakeApp :  renderScreen()
    SdlSnakeApp :  close()
   
    Snake : processEvents()
    Snake : move()

    Utils : checkCollision()
@enduml
