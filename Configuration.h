#ifndef _configuration_h_
#define _configuration_h_

class Configuration
{
    private:
        /* */
        int numOfTilesX = 17;
        /* */
        int numOfTilesY = 17;
        /* */
        int tileSize    = 57;

    public:
        /* Returns the window width in pixel. */
        int getWinWidth()  { return (numOfTilesX * tileSize); }
        /* Returns the window height in pixel. */
        int getWinHeight() { return (numOfTilesY * tileSize); }    
};

#endif // _configuration_h_