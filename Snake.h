#ifndef _snake_h_
#define _snake_h_

#include "Configuration.h"

// The snake that will move around on the screen
class Snake
{
    public:
		// The minimum dimension of the snake.
		static const int SNAKE_MIN_WIDTH  = 1;
		static const int SNAKE_MIN_HEIGHT = 1;

		// Maximum axis velocity of the snake
		static const int SNAKE_VEL = 10;

		// Initializes the variables
		Snake(Configuration& refConfiguration);

		// Restarts the snake for a new game.
		void restartSnake();

		// Sets the size of the snake's head collider. 
		void setSnakeHeadSize( int w, int h);

		// Takes key presses and adjusts the snake's velocity
		void processEvents( SDL_Event& e );

		// Moves the snake and checks collision
		void move( SDL_Rect& wall );

		// Shows the snake on the screen
		const SDL_Rect& getCollider();

    private:
        // The app configuration
        Configuration& myAppConfiguration;

		enum EDirection {E_UP, E_DOWN, E_LEFT, E_RIGHT}; 

		// The direction of the snake
		EDirection eDirection;

		// The velocity of the snake
		int mVelX, mVelY;
		
		// Snake's position and collision box
		SDL_Rect mRectHead;
};
#endif // _snake_h_