#include <SDL2/SDL.h>
#include "Snake.h"
#include "Utils.h"

Snake::Snake(Configuration& refConfiguration)
: myAppConfiguration(refConfiguration)
{
	// Set collision box dimension.
	mRectHead.w = SNAKE_MIN_WIDTH;
	mRectHead.h = SNAKE_MIN_HEIGHT;
    mRectHead.x = 0;
    mRectHead.y = 0;

    // Set the members to start of game.
    restartSnake();
}

void Snake::restartSnake()
{
    // Velocity at start of game.
    mVelX = 1;
    mVelY = 1;

    // Direction at start of game.
    eDirection = E_RIGHT;
    
    // Centre the snake on window at start of game.
    mRectHead.x = (myAppConfiguration.getWinWidth()  - mRectHead.w) / 2;
    mRectHead.y = (myAppConfiguration.getWinHeight() - mRectHead.h) / 2;
}

void Snake::processEvents( SDL_Event& e )
{
    switch( e.key.keysym.sym )
    {
        case SDLK_UP:    eDirection = E_UP;    break;
        case SDLK_DOWN:  eDirection = E_DOWN;  break;
        case SDLK_LEFT:  eDirection = E_LEFT;  break;
        case SDLK_RIGHT: eDirection = E_RIGHT; break;
        default: /* Nothing to do. All possible cases are handled above. */ break;
    }
}

void Snake::move( SDL_Rect& wall )
{
    //Move the snake left or right
    switch(eDirection)
    {
        case E_UP:     mRectHead.y -= mVelY; break;
        case E_DOWN:   mRectHead.y += mVelY; break;
        case E_LEFT:   mRectHead.x -= mVelX; break;
        case E_RIGHT:  mRectHead.x += mVelX; break;
        default: /* Nothing to do. All possible cases are handled above. */ break;
    }

	mRectHead.x = mRectHead.x;
    mRectHead.y = mRectHead.y;

    // If the snake collided or went too far to the left or right
    if( ( mRectHead.x < 0 ) 
    ||  ( mRectHead.x + mRectHead.w > myAppConfiguration.getWinWidth() ) 
    ||  (Utils::checkCollision( mRectHead, wall ))
    )
    {
        //Move back
        mRectHead.x -= mVelX;
		mRectHead.x = mRectHead.x;
    }

    //Move the snake up or down
  	mRectHead.y = mRectHead.y;

    //If the snake collided or went too far up or down
    if( ( mRectHead.y < 0 ) || ( mRectHead.y + mRectHead.h > myAppConfiguration.getWinHeight() ) || Utils::checkCollision( mRectHead, wall ) )
    {
        //Move back
        mRectHead.y -= mVelY;
		mRectHead.y = mRectHead.y;
    }
}

void Snake::setSnakeHeadSize(int w, int h)
{
    mRectHead.w = w;
    mRectHead.h = h;
}

const SDL_Rect& Snake::getCollider()
{
    return mRectHead; 
}