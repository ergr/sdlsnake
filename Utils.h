#ifndef _utils_h_
#define _utils_h_

#include <SDL2/SDL.h>

class Utils
{
    public:
        static bool checkCollision( SDL_Rect a, SDL_Rect b );
};

#endif // _utils_h_