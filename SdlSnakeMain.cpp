#ifdef __cplusplus
#include <cstdlib>
#else
#include <stdlib.h>
#endif

#include <SDL2/SDL.h>
#include "Configuration.h"
#include "SdlSnakeApp.h"

int gTimerTicks;
enum EDirection
{
    E_UP,
    E_DOWN,
    E_LEFT,
    E_RIGHT
};

EDirection gDirection = E_RIGHT;

SDL_Rect gRectDist;

/**
 * @brief Execute the main function as entry point to start the SdlSnake application.
 * 
 * @param argc  The argument counter contains the number of parameters provided by the user. 
 * @param argv  The argument vector contains the parameters as a array of zero terminated strings provided by the user of this application.
 * @return int  Returnvalue as described in the following table:
 *              | #  | reason          | state    |
 *              |---:|:----------------|:--------:|
 *              |  0 | no error        | success  | 
 *              | -1 | init()          | failed   |
 *              | -2 | loadMedia()     | failed   |
 *              | -3 | processEvents() | failed   |
 *              | -4 | updateGame()    | failed   |
 *              | -5 | renderScreen()  | failed   | 
 * 
 *  @startuml
 *     User  -> Snake  : press key
 *     Snake <- User   : change direction
 *  @enduml
 * 
 * TODO: The next line is just an example to evaluate the doxygen capabilities of using dia 
 *  @diafile zEvalDiagram.dia
 * 
 *  @dot
 *  digraph G {
 *   subgraph cluster0 { node [style=filled];
 *   color=grey;
 *   state_init [style=bold,shape=box,peripheries=2,color=black,label="init()"];
 *   state_init -> "loadMedia()" [style=bold];
 *   "loadMedia()" -> "processEvents()\nupdateGame()\nrenderScreen()"
 *   "processEvents()\nupdateGame()\nrenderScreen()" -> "processEvents()\nupdateGame()\nrenderScreen()"
 *   "processEvents()\nupdateGame()\nrenderScreen()" -> "close()"
 *   fontsize=18;label="State Machine"; }
 *  } 
 *  @enddot
 * 
 */
int main(int argc, char *argv[])
{
    int returnValue = 0;

    if (argc != 0)
    {
        printf("OK - Started application %s\n", argv[0]);
    }

    Configuration myAppConfiguration;
    printf("OK - Configuration loaded.\n");

    SdlSnakeApp mySdlSnakeApp(myAppConfiguration, argv[0]);
    printf("OK - SdlSnakeApp created.\n");

    if (!mySdlSnakeApp.init())
    {
        printf("ERROR - init()\n");
        returnValue = -1;
    }
    else
    {
        printf("OK - init() passed.\n");

        if (!mySdlSnakeApp.loadMedia())
        {
            printf("ERROR - loadMedia() failed.\n");
            returnValue = -2;
        }
        else
        {
            printf("OK - loadMedia() passed.\n");

            printf("OK - Start loop: processEvents() -> updateGame() -> renderScreen()\n");

            fflush(stdout);

            while (!mySdlSnakeApp.isQuitGame())
            {
                if (!mySdlSnakeApp.processEvents())
                {
                    printf("ERROR - processEvents() failed.\n");
                    returnValue = -3;
                }
                else
                {
                    if (!mySdlSnakeApp.updateGame())
                    {
                        printf("ERROR - updateGame() failed.\n");
                        returnValue = -4;
                    }
                    else
                    {
                        if (!mySdlSnakeApp.renderScreen())
                        {
                            printf("ERROR - renderScreen() failed.\n");
                            returnValue = -5;
                        }
                    }
                }
            }
        }

        mySdlSnakeApp.close();
    }

    if (returnValue == 0)
    {
        printf("OK - close() SdlSnakeMain exited cleanly.\n");
    }
    else
    {
        printf("ERROR - SdlSnakeMain exit with value %d after failure.\n", returnValue);
    }

    return returnValue;
}
