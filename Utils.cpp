#include <SDL2/SDL.h>
#include "Utils.h"

bool Utils::checkCollision( SDL_Rect a, SDL_Rect b )
{
    //The sides of the rectangles
    int leftA, leftB;
    int rightA, rightB;
    int topA, topB;
    int bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = a.x;
    rightA = a.x + a.w;
    topA = a.y;
    bottomA = a.y + a.h;

    //Calculate the sides of rect B
    leftB = b.x;
    rightB = b.x + b.w;
    topB = b.y;
    bottomB = b.y + b.h;

    //If any of the sides from A are outside of B
    if( bottomA <= topB )
    {
        return false;
    }

    if( topA >= bottomB )
    {
        return false;
    }

    if( rightA <= leftB )
    {
        return false;
    }

    if( leftA >= rightB )
    {
        return false;
    }

    //If none of the sides from A are outside B
    return true;
}

#ifdef UNIT_TESTS
#include "catch2/catch.hpp"

TEST_CASE("Utils checkCollision no collision on zero size")
{
    SDL_Rect a = {0,0,0,0}; // {x,y,width,height}
    SDL_Rect b = {0,0,0,0};

    a.h = 0;
    a.w = 0;
    b.w = 0;
    b.h = 0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.h = 1;
    a.w = 0;
    b.w = 0;
    b.h = 0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.h = 0; 
    a.w = 1;
    b.w = 0;
    b.h = 0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.h = 0; 
    a.w = 0;
    b.w = 1;
    b.h = 0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.h = 0; 
    a.w = 0;
    b.w = 1;
    b.h = 0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.h = 0;
    a.w = 0;
    b.h = 0;
    b.w = 1;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    // Negative values.    
    a.h = -1;
    a.w =  0;
    b.h =  0;
    b.w =  0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.h =  0;
    a.w = -1;
    b.h =  0;
    b.w =  0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.h =  0;
    a.w =  0;
    b.h = -1;
    b.w =  0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.h =  0;
    a.w =  0;
    b.h =  0;
    b.w = -1;
    REQUIRE_FALSE(Utils::checkCollision(a, b));
}

TEST_CASE("Utils checkCollision on size")
{
    SDL_Rect a = {0,0,0,0};
    SDL_Rect b = {0,0,0,0};

    a.h = 1;
    a.w = 1;
    b.h = 1;
    b.w = 1;
    REQUIRE(Utils::checkCollision(a, b));

    a.h = INT32_MAX;
    a.w = 1;
    b.h = 1;
    b.w = 1;
    REQUIRE(Utils::checkCollision(a, b));

    a.h = 1;
    a.w = INT32_MAX;
    b.h = 1;
    b.w = 1;
    REQUIRE(Utils::checkCollision(a, b));

    a.h = 1;
    a.w = 1;
    b.h = INT32_MAX;
    b.w = 1;
    REQUIRE(Utils::checkCollision(a, b));

    a.h = 1;
    a.w = 1;
    b.h = 1;
    b.w = INT32_MAX;
    REQUIRE(Utils::checkCollision(a, b));

    a.h = INT32_MAX;
    a.w = INT32_MAX;
    b.h = INT32_MAX;
    b.w = INT32_MAX;
    REQUIRE(Utils::checkCollision(a, b));
    
}

TEST_CASE("Utils checkCollision no collions on negative size")
{
    SDL_Rect a = {0,0,0,0};
    SDL_Rect b = {0,0,0,0};
    
    a.h = -1;
    a.w =  1;
    b.h =  1;
    b.w =  1;
    REQUIRE_FALSE(Utils::checkCollision(a, b));
    
    a.h =  1;
    a.w = -1;
    b.h =  1;
    b.w =  1;
    REQUIRE_FALSE(Utils::checkCollision(a, b));
    
    a.h =  1;
    a.w =  1;
    b.h = -1;
    b.w =  1;
    REQUIRE_FALSE(Utils::checkCollision(a, b));
    
    a.h =  1;
    a.w =  1;
    b.h =  1;
    b.w = -1;
    REQUIRE_FALSE(Utils::checkCollision(a, b));
    
    a.h = -1;
    a.w = -1;
    b.h = -1;
    b.w = -1;
    REQUIRE_FALSE(Utils::checkCollision(a, b));
    
    a.h = INT32_MIN;
    a.w = -1;
    b.h = -1;
    b.w = -1;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.h = -1;
    a.w = INT32_MIN;
    b.h = -1;
    b.w = -1;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.h = -1;
    a.w = -1;
    b.h = INT32_MIN;
    b.w = -1;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.h = -1;
    a.w = -1;
    b.h = -1;
    b.w = INT32_MIN;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.h = INT32_MIN;
    a.w = INT32_MIN;
    b.h = INT32_MIN;
    b.w = INT32_MIN;
    REQUIRE_FALSE(Utils::checkCollision(a, b));
}

TEST_CASE("Utils checkCollision - no collision positions.")
{
    // Initialize with size to one
    SDL_Rect a = {0,0,1,1};
    SDL_Rect b = {0,0,1,1};

    // Show that the initialization values collide (collision = true).
    REQUIRE(Utils::checkCollision(a, b));

    // The following positions shall not collide.
    a.x = 0;
    a.y = 1;
    b.x = 0;
    b.y = 0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.x = 0;
    a.y = 1;
    b.x = 0;
    b.y = 0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.x = 0;
    a.y = 0;
    b.x = 1;
    b.y = 0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.x = 0;
    a.y = 0;
    b.x = 0;
    b.y = 1;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.x = INT32_MAX;
    a.y = 0;
    b.x = 0;
    b.y = 0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.x = 0;
    a.y = INT32_MAX;
    b.x = 0;
    b.y = 0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.x = 0;
    a.y = 0;
    b.x = INT32_MAX;
    b.y = 0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.x = 0;
    a.y = 0;
    b.x = 0;
    b.y = INT32_MAX;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    // Negative values

    a.x = -1;
    a.y =  0;
    b.x =  0;
    b.y =  0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.x =  0;
    a.y = -1;
    b.x =  0;
    b.y =  0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.x =  0;
    a.y =  0;
    b.x = -1;
    b.y =  0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.x =  0;
    a.y =  0;
    b.x =  0;
    b.y = -1;

    REQUIRE_FALSE(Utils::checkCollision(a, b));
    a.x = INT32_MIN;
    a.y =         0;
    b.x =         0;
    b.y =         0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.x =         0;
    a.y = INT32_MIN;
    b.x =         0;
    b.y =         0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.x =         0;
    a.y =         0;
    b.x = INT32_MIN;
    b.y =         0;
    REQUIRE_FALSE(Utils::checkCollision(a, b));

    a.x =         0;
    a.y =         0;
    b.x =         0;
    b.y = INT32_MIN;
    REQUIRE_FALSE(Utils::checkCollision(a, b));
}

TEST_CASE("Utils checkCollision - collision positions.")
{
    // Initialize with minimum size. 
    SDL_Rect a = {0,0,1,1};
    SDL_Rect b = {0,0,1,1};
    
    a.x =  0;
    a.y =  0;
    b.x =  0;
    b.y =  0;
    REQUIRE(Utils::checkCollision(a, b));

    a.x =  1;
    a.y =  0;
    b.x =  1;
    b.y =  0;
    REQUIRE(Utils::checkCollision(a, b));

    a.x =  0;
    a.y =  1;
    b.x =  0;
    b.y =  1;
    REQUIRE(Utils::checkCollision(a, b));

    // These values show the effect of  
    a.x =  INT32_MAX-1;
    a.y =  0;
    b.x =  INT32_MAX-1;
    b.y =  0;
    REQUIRE(Utils::checkCollision(a, b));

    a.x =  0;
    a.y =  INT32_MAX-1;
    b.x =  0;
    b.y =  INT32_MAX-1;
    REQUIRE(Utils::checkCollision(a, b));

    a.x =  INT32_MAX-1;
    a.y =  INT32_MAX-1;
    b.x =  INT32_MAX-1;
    b.y =  INT32_MAX-1;
    REQUIRE(Utils::checkCollision(a, b));

    // Negative values.
    a.x =  -1;
    a.y =   0;
    b.x =  -1;
    b.y =   0;
    REQUIRE(Utils::checkCollision(a, b)); 

    a.x =   0;
    a.y =  -1;
    b.x =   0;
    b.y =  -1;
    REQUIRE(Utils::checkCollision(a, b)); 

    a.x = INT32_MIN+1;
    a.y =         0;
    b.x = INT32_MIN+1;
    b.y =         0;
    REQUIRE(Utils::checkCollision(a, b)); 

    a.x =         0;
    a.y = INT32_MIN+1;
    b.x =         0;
    b.y = INT32_MIN+1;
    REQUIRE(Utils::checkCollision(a, b)); 

    a.x = INT32_MIN+1;
    a.y = INT32_MIN+1;
    b.x = INT32_MIN+1;
    b.y = INT32_MIN+1;
    REQUIRE(Utils::checkCollision(a, b)); 
}
#endif
