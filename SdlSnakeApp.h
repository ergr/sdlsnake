#ifndef _sdlSnakeApp_h_
#define _sdlSnakeApp_h_

#include <SDL2/SDL.h>
#include "Configuration.h"
#include "Snake.h"

/**
 * @brief The snake application organizes the high level objects of this game.
 * 
 */
class SdlSnakeApp
{
public:
    /**
     * @brief Construct a new Sdl Snake App object.
     * 
     * @param refConfiguration It must have always a valid configuration.
     * @param szTitle This is the title of the window.
     */
    SdlSnakeApp(Configuration& refConfiguration, const char* szTitle);

    // Initializes the SDL and creates windows and its surface and renderer.
    bool init();

    // Loads all used media and generates their surfaces and textures.
    bool loadMedia();

    // Handles all input and timer events.
    bool processEvents();

    // Process the game logic and moves the objects.
    bool updateGame();

    // Renders and updates all scenes to the screen.
    bool renderScreen();

    // Closes all resources and returns them to the operating system.
    void close();

    // True if new game.
    bool isNewGame()  { return bNewGame;  };

    // True if new game.
    bool isQuitGame() { return bQuitGame; };

private:
    // The used configuration.
    Configuration& myConfiguration;

    // The window we'll be rendering to.
    SDL_Window* myWindow = nullptr;

    // The main window's surface.
    SDL_Surface* mySurfaceWinodow = nullptr;

    // The snake's surface.
    SDL_Surface* mySurfaceSnake = nullptr;

    // The snake.
    Snake mySnake;

    // The food.
    SDL_Rect myFood;

    // 
    bool bNewGame = true;

    //
    bool bQuitGame = false;

    // 
    const char* szAppTitle; 
};

#endif // _sdlSnakeApp_h_