# Evaluate PlantUML with Doxygen using VSCode and Markdown

At the current time, the doxygen implementation and its PlantUML capabilities are incompatible to the used VSCode Markdown extensions in the way of using the fenced code block syntax.

## PlantUML sequence diagrams
The next lines surrounded with triple back-quotes \`\`\` are used for the vscode extension `plantuml`

```plantuml
@startuml {Scenario_0_-_Snake_Utils_checkCollision} "Scenario 0 - Snake Utils - checkCollision" width=5cm
    Snake -> Utils : checkCollision()
@enduml
```

The next lines with the `@startuml` will produce the same PlantUML figure, but as result from doxygen.

@startuml {Scenario_0_-_Snake_Utils_checkCollision} "Scenario 0 - Snake Utils - checkCollision" width=5cm
    Snake -> Utils : checkCollision()
@enduml

```plantuml
\startuml {Scenario_0_-_Snake_Utils_checkCollision} "Scenario 0 - Snake Utils - checkCollision" width=5cm
    Snake -> Utils : checkCollision()
\enduml

```

The next lines with the `\startuml` will produce the same PlantUML figure, but as result from doxygen.

\startuml {Scenario_0_-_Snake_Utils_checkCollision} "Scenario 0 - Snake Utils - checkCollision" width=5cm
    Snake -> Utils : checkCollision()
\enduml

## PlantUML class diagrams

The next example evaluates the class diagram.
Class diagrams in PlantUML need the `dot` tool provided by the graphviz package/project.

```plantuml
\startuml {DoxygenClassExample} "Example of Doxygen Class diagram." 
    Vehicle <|.. Car : extended by >
\enduml
```

\startuml {DoxygenClassExample} "Example of Doxygen Class diagram." 
    Vehicle <|.. Car : extended by >
\enduml

The next lines evaluate the PlantUML version and the environment according the dot tool.

```plantuml
@startuml
testdot
@enduml
```

@startuml
testdot
@enduml

```plantuml
@startuml
version
@enduml
```

@startuml
version
@enduml
