# MS Windows with MSYS2 MINGW64 and VSCode

## MINGW64

### MSYS2 installation

On MS Windows the MSYS2 MINGW64 environment is used. from https://www.msys2.org/ download the 64bit version msys2-x86_64-YYYYMMDD.exe

Follow the installation from the official website.

### MINGW64 required tools and libraries

    pacman -S mingw-w64-x86_64-gcc mingw-w64-x86_64-libSDL2 mingw-w64-x86_64-libSDL2_image mingw-w64-x86_64-clang-tools-extra mingw-w64-x86_64-cppcheck

### bash preparetion

Add to your `.bahsrc` in the home directory `/home/<username>/` aka `c:/msys64/home/<username>`

~~~ bashrc
    export PATH=/mingw64/bin:/usr/local/bin:/usr/bin:/bin:/c/Windows/System32:/c/Windows:/c/Windows/System32/Wbem:/c/Windows/System32/WindowsPowerShell/v1.0/:/usr/local/jdk-14/bin
    export JAVA_HOME=/usr/local/jdk-14
~~~

### doxygen preparation

In the doxygen.conf file:
Set the `PLANTUML_JAR_PATH      = /usr/local/share/plantuml/plantuml.jar`

### plantuml

Copy the plantuml.jar to /usr/local/share/plantuml/plantuml.jar. This is a common used searchpath, e.h. by often used cmake scripts.

## VSCode - user settings

at `C:\Users\<username>\AppData\Roaming\Code\User\settings.json`
or `CTRL+SHIFT+p` "Preferences: Open Settings (JSON)"

add the following lines:

~~~ json
"terminal.integrated.shell.windows": "C:\\msys64\\usr\\bin\\bash.exe",
"terminal.integrated.shellArgs.windows": [
    "--login",
    "-i"
],
"terminal.integrated.env.windows": {
    "MSYSTEM": "MINGW64",
    "CHERE_INVOKING": "1",
},
~~~
