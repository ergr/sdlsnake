# EXEC_BASE_NAME specifies the name of our exectuable
EXEC_BASE_NAME = SdlSnake
MAIN_OF_APPLICATION = SdlSnakeMain.cpp
MAIN_OF_UNITTEST = testmain.cpp

# This lists all cpp files.
CPP = $(wildcard *.cpp)
CPP_BUILD = $(filter-out $(MAIN_OF_UNITTEST),$(CPP))
CPP_TEST  = $(filter-out $(MAIN_OF_APPLICATION),$(CPP))

# Build directory
BUILD_DIR = build

# Test directory
TEST_DIR = build/test

# All .o files go to build dir.
OBJ_BUILD = $(CPP_BUILD:%.cpp=$(BUILD_DIR)/%.o)

# All .o files go to test dir.
OBJ_TEST = $(CPP_TEST:%.cpp=$(TEST_DIR)/%.o)

# Gcc/Clang will create these .d files containing dependencies.
DEP_BUILD = $(OBJ_BUILD:%.o=%.d)

# Gcc/Clang will create these .d files containing dependencies.
DEP_TEST  = $(OBJ_TEST:%.o=%.d)

#CC specifies which compiler we're using
CC = g++
INCLUDE_PATHS  = -I/usr/include/SDL2
CXXFLAGS = -c -g -Wall -Wextra -pedantic
LDFLAGS  = 
LDLIBS   = -lSDL2

ifdef MSYSTEM
CC              =  $(MSYSTEM_PREFIX)/bin/clang++.exe
INCLUDE_PATHS   = -I$(MSYSTEM_PREFIX)/include/SDL2
# -w suppresses all warnings
# -Wl,-subsystem,windows gets rid of the console window
CXXFLAGS  = -g -std=c++17 -Wall -Wextra -pedantic -w -Wl,-subsystem,windows
LDFLAGS   = -L$(MSYSTEM_PREFIX)/lib
LDLIBS    = -lmingw32 -lSDL2main -lSDL2 -lSDL2_image
EXEC_FULL_NAME      = $(EXEC_BASE_NAME)_$(MSYSTEM_CARCH).exe
EXEC_TEST_FULL_NAME = $(EXEC_BASE_NAME)_TEST_$(MSYSTEM_CARCH).exe
else
EXEC_FULL_NAME      = $(EXEC_BASE_NAME)
EXEC_TEST_FULL_NAME = $(EXEC_BASE_NAME)_TEST
endif

ifdef XDG_SESSION_TYPE
START = xdg-open
else
START = start
endif

# This test if you run MSYS and neither MINGW64 nor MINGW32
ifeq ($(MSYSTEM), MSYS) 
error: 
	@echo "You are running MSYS. This is a not supported environment. Use MINGW32 or MINGW64"
endif

# Default target - named by the executable
$(EXEC_FULL_NAME) : $(BUILD_DIR)/$(EXEC_FULL_NAME)

all: $(EXEC_FULL_NAME) check doc

# The actual target of the executable - depends on all .o files.
$(BUILD_DIR)/$(EXEC_FULL_NAME) : $(OBJ_BUILD)
	@# Create the build directory.
	@mkdir -p $(@D)
	# Link all the objects to the final executable.
	$(CC) $^ $(LDLIBS) $(LDFLAGS) -o $@
	
# Include all .d files
-include $(DEP)

# Build target for every single object file.
# The potential dependency on header files is covered by calling -include $(DEP)
$(BUILD_DIR)/%.o : %.cpp
	@# Create the build directory.
	@mkdir -p $(@D)
	$(CC) $(CXXFLAGS) $(INCLUDE_PATHS) -MMD -c $< -o $@ 
	
.PHONY : clean

doc : $(BUILD_DIR)/doc/index.html

$(BUILD_DIR)/doc/index.html:
	# Build the doxgen documentation
	/usr/bin/doxygen doxygen.conf
	# Start html viewer/browser
	$(START) $(BUILD_DIR)/doc/html/index.html

clean :
	# Remove all derived objects in the build folder.
	rm -rf build

# Target for the unittests
check : $(TEST_DIR)/$(EXEC_TEST_FULL_NAME)
	# Run the unittest suite
	./$(TEST_DIR)/$(EXEC_TEST_FULL_NAME)

# The actual target of the executable - depends on all .o files.
$(TEST_DIR)/$(EXEC_TEST_FULL_NAME) : $(OBJ_TEST)
	@# Create the test directory.
	@mkdir -p $(@D)
	# Link all unittest objects to one test suite executable.
	$(CC) $^ $(LDLIBS) $(LDFLAGS) -o $@
	
# Include all .d files for test
-include $(DEP_TEST)

# Build target for every single object file.
# The potential dependency on header files is covered by calling -include $(DEP)
$(TEST_DIR)/%.o : %.cpp
	@# Create the build directory.
	@mkdir -p $(@D)
	$(CC) -DUNIT_TESTS $(CXXFLAGS) $(INCLUDE_PATHS) -MMD -c $< -o $@ 

debug obj_test:
	@echo "OBJ_TEST_TEMP = "$(OBJ_TEST_TEMP)
	@echo "OBJ_TEST      = "$(OBJ_TEST)
